# Copyright Kikan 2020
# File created by Benjamin Paccaud - 09.06.20 <bpaccaud@gmail.com>

from . import entity
from . import json_interface
from .tools import Date
from .tools import error
from .tools import kikanuuid


class Child(entity.AbstractEntity, json_interface.JsonInterface):
    """
    A Child is the repository of all the information on it and its appartenance to a group and
    its presence across time.
    """
    VERSION = 1

    def __init__(self, infos, date_of_birth, group, presences, _id):
        super(Child, self).__init__(_id)
        self.dob = date_of_birth
        self.infos = infos
        self.group = group
        self.presences = presences

    def __str__(self):
        return f"Child({self.id}, {self.infos})"

    @staticmethod
    def create_new(infos, date_of_birth, first_group, first_presences, date):
        return Child(infos, date_of_birth, first_group, first_presences, kikanuuid.get_id())

    def to_dict(self):
        return {
            json_interface.VERSION_KEY: Child.VERSION,
            "dob": self.dob.to_dict(),
            "infos": self.infos,
            "group": self.group.to_dict(),
            "presences": [p.to_dict() for p in self.presences],
            "id": self.id
        }

    @staticmethod
    def from_dict(json_dict):
        version = json_dict.get(json_interface.VERSION_KEY)
        if version == Child.VERSION:
            dob = Date.from_dict(json_dict.get("dob"))
        else:
            raise error.CoreError(error.ErrorCode.INTERFACE_VERSION, f"Unknown version: {version}")


