# Copyright Kikan 2020
# File created by Benjamin Paccaud - 05.06.20 <bpaccaud@gmail.com>


class Day:
    """
    Represents an individual day in a week
    """

    def __init__(self, parts_day):
        self.part_day = parts_day
