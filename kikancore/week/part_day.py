# Copyright Kikan 2020
# File created by Benjamin Paccaud - 05.06.20 <bpaccaud@gmail.com>


class PartDay:
    """
    Represent a subdivision of a Day
    """

    def __init__(self, name, presences):
        self.name = name
        self.presences = presences
