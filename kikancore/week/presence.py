# Copyright Kikan 2020
# File created by Benjamin Paccaud - 05.06.20 <bpaccaud@gmail.com>
from ..tools import log

logger = log.get_logger("presence")


class Presence:
    """Represents the options for the presence in a PartDay.
    Contains has an Id that is related to a file with the actual data
    """

    def __init__(self, _id):
        self._id = _id
        logger.debug("Created Presence with id (%s)", self._id)

    def __str__(self):
        return f"Presence({self._id})"
