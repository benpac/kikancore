# Copyright Kikan 2020
# File created by Benjamin Paccaud - 05.06.20 <bpaccaud@gmail.com>


class Week:
    """
    Represents a ensemble of days in a particular order
    """

    def __init__(self, days):
        self.days = days
