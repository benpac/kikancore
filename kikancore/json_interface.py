# Copyright Kikan 2020
# File created by Benjamin Paccaud - 10.06.20 <bpaccaud@gmail.com>


"""
This file provide an Interface for every object that should be saved to disk in a json format.

It also provides helper functions to dump and load from the json module
"""

import abc
import json

VERSION_KEY = "version"


class JsonInterface(abc.ABC):
    @abc.abstractmethod
    def to_dict(self):
        return {}

    @staticmethod
    @abc.abstractmethod
    def from_dict(json_dict):
        return None

    @staticmethod
    def from_json(json_str):
        return from_json(json_str)

    def to_json(self):
        return to_json(self.to_dict())


def to_json(d):
    return json.dumps(d, indent=2)


def from_json(s):
    return json.loads(s)


def get_json_dict_values(keys, json_dict):
    values = []
    for key in keys:
        if key not in json_dict:
            raise KeyError("Missing key '" + key + "' from dictionary.")
        values.append(json_dict.get(key))
    return values

