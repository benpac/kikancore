# Copyright Kikan 2020
# File created by Benjamin Paccaud - 10.06.20 <bpaccaud@gmail.com>

import uuid

"""
This files provides tools relative to creating and comparing uuid for the various objects in the 
list.
"""


def get_id():
    """Get a new uuid

    :return: a UUID version 4 32-byte string of hex representation.
    """
    return uuid.uuid4().hex


