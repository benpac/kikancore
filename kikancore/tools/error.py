# Copyright Kikan 2020
# File created by Benjamin Paccaud - 09.06.20 <bpaccaud@gmail.com>

import enum


class ErrorCode(enum.Enum):
    """
    Enum class containing all the error codes.
    Enum numbers are define with Python's enum.auto(). Please verify the version of kikancore you
    are using before comparing error code values.
    """
    UNDEFINED = enum.auto()

    INTERFACE_VERSION = enum.auto()
    INTERFACE_MISSING_KEY = enum.auto()
    INTERFACE_BAD_VALUE = enum.auto()
    INTERFACE_INVALID_CLASS = enum.auto()

    PERCENT_BAD_VALUE = enum.auto()


class CoreError(Exception):
    """
    Base Exception class for kikancore.
    """

    def __init__(self, _id, *args):
        super(CoreError, self).__init__(*args)
        self._id = _id

    def __str__(self):
        message = self.args
        return f"{message}, code:{self._id}"
