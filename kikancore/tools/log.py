# Copyright Kikan 2020
# File created by Benjamin Paccaud - 06.04.20 <bpaccaud@gmail.com>
#
import logging
import logging.handlers
import os

from . import directories

"""
This module is meant to be used to log message. Will print anything from INFO up to 
the console and all the messages to the file provided.

Simply import this module at the beginning of the file you want to log and create a logger with 
the function get logger. The logger can then be used as the logging module would be (see logging 
package documentation for more information):

>> import tools.log
>>
>> logger = tools.log.get_logger("log name", "path/to/log/file)
>> logger.info("started logging")
>> logger.debug("debug info")
>> logger.error("Something bad happened")

"""
GLOBAL_LOG_PATH = directories.log_directory() / "core.log"


def get_logger(name: str, filepath=GLOBAL_LOG_PATH, filemode='a'):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    os.makedirs(os.path.dirname(filepath), exist_ok=True)
    file_handler = logging.handlers.RotatingFileHandler(
        filepath, mode=filemode, maxBytes=int(1e7), backupCount=10)
    file_formatter = logging.Formatter(fmt="{asctime} - {levelname:8}: {message}", style="{")
    file_handler.setFormatter(file_formatter)
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    console_formatter = logging.Formatter('%(levelname)-8s: %(message)s')
    console.setFormatter(console_formatter)
    logger.addHandler(console)
    logger.addHandler(file_handler)
    return logger
