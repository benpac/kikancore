# Copyright Kikan 2020
# File created by Benjamin Paccaud - 04.06.20 <bpaccaud@gmail.com>
import reprlib

from . import error
from .date import Date
from .time_node import TimeNode
from .. import json_interface


class TimeList(json_interface.JsonInterface):
    """
    This class is a linked list containing a start date value in (YYYY-MM-DD) format and the actual
    object of interest. It is meant to be an abstract class and each subclass must implement the at()
    function that returns the value at a specific date. An internal function _at() will return the
    object at the date to help. This allows some subclass to decide if they need to perform extra
    actions on the object before returning.
    """

    JSON_LIST_KEY = "tl"
    JSON_TYPE_KEY = "type"

    BASE_TYPE_MAP = {
        type(base_type): base_type for base_type in [str, int, float, bool, list, dict]
    }

    def __init__(self, value_type):
        """
        :param value_type: The type() of the values that should be in the list. Every subsequent
        addition to the list will be required to be of this type
        """
        if value_type is None:
            raise TypeError("TimeList requires a valid type")
        self.type = value_type
        self.list = []

    def __str__(self):
        return f"TimeList({reprlib.repr(self.list), self.type})"

    def set(self, date, value):
        if not isinstance(value, self.type):
            raise TypeError(
                "Cannot add value of type " + str(type(value)) +
                " to a TimeList made for " + str(self.type))
        node = TimeNode(date, value)
        self.list.append(node)
        self._consolidate()

    def _set_list(self, new_list):
        """Set a whole list. Use by from_json.

        :param new_list: a list of TimeNode
        """
        if any([not isinstance(time_node, TimeNode) for time_node in new_list]):
            raise TypeError("Can only set list of TimeNodes")
        if any([not isinstance(time_node.value, self.type)] for time_node in new_list):
            raise TypeError("Can only set list of TimeNode with values of type " + str(self.type))
        self.list = new_list
        self._consolidate()

    def _consolidate(self):
        """Reorders the elements and check if there are instances where a changes does nothing (
        changing a value to the exact same value)

        """
        self.list.sort(key=lambda n: n.date)
        changed = True
        while changed:
            changed = False
            index = 0
            while index < len(self.list)-1:
                if self.list[index].date == self.list[index+1].date:
                    del self.list[index]
                    changed = True
                elif self.list[index].value == self.list[index + 1].value:
                    del self.list[index+1]
                    changed = True
                else:
                    index += 1

    def _at(self, date):
        """Internal function to return the object at date.

        :param date: The Date at which we want the value.
        :return: The value stored at the closest node before date
        """
        if not isinstance(date, Date):
            raise TypeError("Invalid Date")
        if len(self.list) == 0:
            return None
        if date < self.list[0].date:
            return None
        for index, n in enumerate(self.list):
            if date < n.date:
                return self.list[index-1].value
        else:
            return self.list[-1].value

    def at(self, date):
        """Identical to the internal function _at, but can be overridden by subclasses to perform
        some operation on the object before returning.

        :param date: The Date at which we want the value.
        :return: The value stored at the closest node before date
        """
        return self._at(date)

    # region JsonInterface

    def to_dict(self):
        return {
            TimeList.JSON_LIST_KEY: [n.to_dict() for n in self.list],
            TimeList.JSON_TYPE_KEY: str(self.type)
        }

    @staticmethod
    def from_dict(json_dict, value_class=None):
        """The from_json needs the value_class to create the values correctly.

        :param json_dict: a dictionary coming from a json file
        :param value_class: the class to create the value. Must implement
        json_interface.JsonInterface.
        :return: a TimeList
        """
        time_list_dict_list, time_list_type = json_interface.get_json_dict_values([
            TimeList.JSON_LIST_KEY, TimeList.JSON_TYPE_KEY], json_dict)
        if not isinstance(time_list_dict_list, list):
            raise error.CoreError(
                error.ErrorCode.INTERFACE_BAD_VALUE, "Json for TimeList does not contain a list.")

        time_nodes = [TimeNode.from_dict(n, value_class) for n in time_list_dict_list]
        time_list = TimeList(TimeList.BASE_TYPE_MAP.get(time_list_type, value_class))
        time_list._set_list(time_nodes)
        return time_list

    # endregion
