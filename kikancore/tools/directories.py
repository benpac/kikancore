# Copyright Kikan 2020
# File created by Benjamin Paccaud - 09.06.20 <bpaccaud@gmail.com>
import pathlib

"""
This file provides a centralized place to access directories and files related to Kikan. This is 
specific to the Core.
"""

KIKAN_FOLDER = ".kikan"


def core_directory():
    home_dir = pathlib.Path.home()
    return pathlib.Path(home_dir, KIKAN_FOLDER)


def log_directory():
    return pathlib.Path(core_directory(), "logs")
