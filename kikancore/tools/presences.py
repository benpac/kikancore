# Copyright Kikan 2020
# File created by Benjamin Paccaud - 05.06.20 <bpaccaud@gmail.com>
import json

"""This file is used to read a list of user defined presences options"""


def presence_dict(filepath):
    with open(filepath, 'r') as presence_file:
        presences_definition = json.load(presence_file)

    return presences_definition
