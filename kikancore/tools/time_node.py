from .. import json_interface
from ..tools import Date
from ..tools import error


class TimeNode(json_interface.JsonInterface):
    """
    A TimeNode is a linked list node that contains a date, an object and the next value.
    """
    ACCEPTED_TYPES = (str, int, float, bool, dict, list)

    JSON_DATE = "d"
    JSON_VALUE = "v"

    def __init__(self, date, value):
        """
        Constructor

        :param date: a Date.
        :param value: anything that can be saved and restored to json format or a JsonInterface.
        """
        if not isinstance(date, Date):
            raise TypeError("TimeNode date must be a Date!")
        self.date = date
        if value is not None and not isinstance(
                value, (json_interface.JsonInterface, *self.ACCEPTED_TYPES)):
            raise TypeError("TimeNode value must be basic type or implement JsonInterface!")
        self.value = value

    def __str__(self):
        return f"({self.date} -> {self.value})"

    def __repr__(self):
        return self.__str__()

    # region JsonInterface

    def to_dict(self):
        """TimeNode will correctly send to_dict any value that is already acceptable by a json
        dictionary. If the value is a json_interface.JsonInterface, it will call to_dict() on it.

        :return:
        """
        if isinstance(self.value, self.ACCEPTED_TYPES) or self.value is None:
            value_dict = self.value
        else:  # it is a JsonInterface
            value_dict = self.value.to_dict()

        return {
            TimeNode.JSON_DATE: self.date.to_dict(),
            TimeNode.JSON_VALUE: value_dict
        }

    @staticmethod
    def from_dict(json_dict, value_class=None):
        """The from_json of this class must know about the class of the value to create it from
        the json dictionary

        :param json_dict: a dictionary coming from a json file
        :param value_class: the class to create the value. Must implement
        json_interface.JsonInterface.
        :return:
        """
        date_dict, value = json_interface.get_json_dict_values(
            (TimeNode.JSON_DATE, TimeNode.JSON_VALUE), json_dict)
        date = Date.from_dict(date_dict)

        if value_class is not None and not issubclass(value_class, json_interface.JsonInterface):
            raise error.CoreError(
                error.ErrorCode.INTERFACE_INVALID_CLASS,
                str(value_class) + " does not implements " + str(json_interface.JsonInterface))
        if value_class is not None and issubclass(value_class, json_interface.JsonInterface):
            if isinstance(value, list) and all([isinstance(v, dict) for v in value]):
                value = [value_class.from_dict(v) for v in value]
            elif isinstance(value, dict):
                value = value_class.from_dict(value)
            else:
                raise TypeError("Expected 'dict' or 'list' to recreate object of class " +
                                str(type(value_class)))
        return TimeNode(date, value)

    # endregion
