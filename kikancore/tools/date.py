import datetime

from .. import json_interface
from ..tools import error


class Date(datetime.date, json_interface.JsonInterface):
    """
    Wrapper class around datetime.date to be able to provide only year and month in the
    constructor
    """
    JSON_KEY = "ymd"

    def __new__(cls, year, month, day=1):
        return datetime.date.__new__(cls, year, month, day)

    def to_dict(self):
        return {Date.JSON_KEY: [self.year, self.month, self.day]}

    @staticmethod
    def from_dict(json_dict):
        ymd, = json_interface.get_json_dict_values([Date.JSON_KEY], json_dict)
        if not isinstance(ymd, (list, tuple)) or len(ymd) != 3:
            raise error.CoreError(error.ErrorCode.INTERFACE_BAD_VALUE, Date.JSON_KEY)
        else:
            return Date(*ymd)
