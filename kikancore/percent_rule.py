# Copyright Kikan 2020
# File created by Benjamin Paccaud - 09.06.20 <bpaccaud@gmail.com>

import abc

from .tools import error


class PercentRule(abc.ABC):
    """
    A Percent rule is a class performing the computation of the precentage of presence of a child
    at a given week.

    This class is meant to be the base abstract class for the specific type of rules implementing
    it.
    """

    @abc.abstractmethod
    def compute_percent(self, presences):
        raise NotImplementedError()


class PercentRuleByPart(PercentRule):
    def __init__(self, values):
        """Constructor.

        :param values: a List of values corresponding to the "percent" of a full week for each
        part of day.
        """
        self.values = values

    def compute_percent(self, presences):
        """Computes the percent of presence based on the given list of presences.
        Will perform the simple product of the presence value (either 0 or 1) with the individual
        values in self.values and sum them.

        :param presences: list of Presence
        :return: the computed percent.
        """
        if len(presences) != len(self.values):
            raise error.CoreError(
                error.ErrorCode.PERCENT_BAD_VALUE, "Invalid size.")

        percent = sum(
            [presence.value * value for presence, value in zip(presences, self.values)])

        return percent


class PercentRuleByMask(PercentRule):
    def __init__(self, masks):
        """Constructor.

        :param masks: a dictionary of mask - percent value.
        """
        self.masks = masks

    def compute_percent(self, presences):
        """Computes the percent of presence by comparing presences to the masks.

        :param presences: list of Presence
        :return: the computed percent.
        """
        # So the masks should probably be defined by day and one could reuse a mask for one day
        # for all the others. Otherwise this would probably
        raise NotImplementedError
