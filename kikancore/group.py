# Copyright Kikan 2020
# File created by Benjamin Paccaud - 09.06.20 <bpaccaud@gmail.com>
from . import entity
from .tools import kikanuuid
from .tools import log

logger = log.get_logger("group")


class Group(entity.AbstractEntity):
    """
    Each child will belong to a group or another at any given point. A group can represent any
    features that are common to multiple child. It is meant that a child can only belong to one
    group at a time.
    """

    def __init__(self, name, _type, data, _id):
        super(Group, self).__init__(_id)
        self.name = name
        self.type = _type
        self.data = data

    @staticmethod
    def create_from_dict(json_dict):
        logger.info("creating group %s", str(json_dict))
        return Group("Group1", "dontknowwhatthisis", {}, kikanuuid.get_id())

    @staticmethod
    def create_new(name: str, _type, data: dict):
        """Creates a new group from only the name, type and data. This will generate a new id
        that will be used for this group.

        :param name: The group's name
        :param _type: I'm not sure what this is
        :param data: dictionary of additional data.
        :return:
        """
        logger.info("Creating new group (%s, %s, %s)", name, _type, data)
        return Group(name, _type, data, kikanuuid.get_id())
