import pytest

import kikancore as kc


class TestCreation:
    def test_with_year_month_day(self):
        date = kc.Date(2020, 6, 25)

    def test_with_year_and_month(self):
        date = kc.Date(2020, 6)

    def test_with_invalid_month_raises(self):
        with pytest.raises(ValueError):
            date_3 = kc.Date(2020, 13, 15)

    def test_with_invalid_day_raises(self):
        with pytest.raises(ValueError):
            date_4 = kc.Date(2020, 12, 35)


def test_date_later_in_the_year_is_greater():
    date_1 = kc.Date(2020, 6, 25)
    date_2 = kc.Date(2020, 6)

    assert date_1 > date_2


def test_date_with_default_day_value_is_equal_to_date_created_explicitly():
    date_default_day = kc.Date(2020, 6)
    date_explicit = kc.Date(2020, 6, 1)

    assert date_default_day == date_explicit


class TestDateJsonInterface:
    def test_to_dict_has_only_one_key_and_the_value_is_a_list_of_year_month_day(self):
        date = kc.Date(2020, 6, 12)
        date_dict = date.to_dict()
        date_dict_expected = {
            kc.Date.JSON_KEY: [2020, 6, 12]
        }
        assert date_dict == date_dict_expected

    def test_from_a_dict_the_created_date_is_equal_to_the_first_one(self):
        date = kc.Date(2020, 6, 12)
        date_dict_expected = {
            kc.Date.JSON_KEY: [2020, 6, 12]
        }
        date_from_dict = kc.Date.from_dict(date_dict_expected)

        assert date_from_dict == date

    @pytest.mark.parametrize("key", ["wrong_key", None, kc.Date(2020, 6, 2), 1])
    def test_wrong_key_in_dict_raises_core_error(self, key):
        with pytest.raises(KeyError):
            date_dict_expected_false = {key: [2020, 6, 12]}
            date = kc.Date.from_dict(date_dict_expected_false)

    @pytest.mark.parametrize("list_date", [[2020, 6], None])
    def test_wrong_list_of_year_month_day_raises_core_error(self, list_date):
        with pytest.raises(kc.error.CoreError):
            date_dict_expected_false = {kc.Date.JSON_KEY: list_date}
            date = kc.Date.from_dict(date_dict_expected_false)

