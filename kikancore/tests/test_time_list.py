# Copyright Kikan 2020
# File created by Benjamin Paccaud - 11.06.20 <bpaccaud@gmail.com>
import pytest

import kikancore as kc


# region TimeList

class TestCreation:
    @pytest.mark.parametrize("value_type, values", [
        (str, ["a", 23]),
        (str, [23]),
        (int, [1, 2, 3, 25.36]),
        (kc.Date, [kc.Date(2020, 12, 12), 23])
    ])
    def test_adding_a_value_of_different_type_raises(self, value_type, values):
        with pytest.raises(TypeError):
            time_list = kc.TimeList(value_type)
            for index, value in enumerate(values):
                time_list.set(kc.Date(2020, 12, 1+index), value)

    @pytest.mark.parametrize("value_type, new_list", [
        (str, [kc.TimeNode(kc.Date(2020, 6), "a"), kc.TimeNode(kc.Date(2020, 7), 1)]),
        (str, [kc.TimeNode(kc.Date(2020, 6), 1), kc.TimeNode(kc.Date(2020, 7), 1)]),
    ])
    def test_setting_a_list_of_different_type_raises(self, value_type, new_list):
        with pytest.raises(TypeError):
            time_list = kc.TimeList(value_type)
            time_list._set_list(new_list)

    def test_setting_a_list_of_non_time_nodes_raises(self):
        with pytest.raises(TypeError):
            time_list = kc.TimeList(int)
            time_list._set_list([1, 2, 3, 4])

    def test_adding_different_element_at_different_date_adds_to_the_length_of_the_list(self):
        time_list = kc.TimeList(int)
        values = list(range(1, 6))
        for value in values:
            time_list.set(kc.Date(2020, value), value)

        assert len(time_list.list) == len(values)

    def test_adding_different_element_at_the_same_date_does_not_lengthen_the_list_and_the_value_is_changed(self):
        time_list = kc.TimeList(int)
        date = kc.Date(2020, 4)
        first_value = 2
        new_value = 4
        time_list.set(date, first_value)
        time_list.set(date, new_value)

        assert len(time_list.list) == 1
        assert time_list.at(date) == new_value

    def test_adding_identical_element_at_successive_dates_does_not_lengthen_the_list_and_first_month_is_kept(self):
        time_list = kc.TimeList(int)
        month_indexes = list(range(1, 6))
        for month_index in month_indexes:
            time_list.set(kc.Date(2020, month_index), 1)

        assert len(time_list.list) == 1
        assert time_list.list[0].date == kc.Date(2020, 1)

    def test_adding_element_at_non_successive_dates_reorders_list_from_earliest(self):
        time_list = kc.TimeList(int)

        time_list.set(kc.Date(2020, 4), 1)
        time_list.set(kc.Date(2020, 5), 2)
        time_list.set(kc.Date(2020, 3), 3)

        assert time_list.list[0].date < time_list.list[1].date
        assert time_list.list[1].date < time_list.list[2].date

    def test_replacing_value_with_one_identical_to_previous_date_shorten_the_list_by_1(self):
        time_list = kc.TimeList(int)
        first_value = 1
        second_month = 4
        time_list.set(kc.Date(2020, 3), first_value)
        time_list.set(kc.Date(2020, second_month), 2)
        time_list.set(kc.Date(2020, 5), 3)
        list_length = len(time_list.list)

        time_list.set(kc.Date(2020, second_month), first_value)

        assert len(time_list.list) == list_length-1
        assert time_list.at(kc.Date(2020, second_month)) == first_value


class TestAt:

    def test_date_before_first_returns_none(self):
        time_list = kc.TimeList(int)
        time_list.set(kc.Date(2020, 2), 5)
        value_before = time_list.at(kc.Date(2020, 1))
        assert value_before is None

    def test_list_empty_returns_none(self):
        time_list = kc.TimeList(int)
        value_empty = time_list.at(kc.Date(2020, 1))
        assert value_empty is None

    def test_date_in_the_list_returns_its_value(self):
        time_list = kc.TimeList(int)
        time_list.set(kc.Date(2020, 2), 4)
        second_date = kc.Date(2020, 3)
        second_value = 5
        time_list.set(second_date, second_value)
        time_list.set(kc.Date(2020, 4), 6)

        value_at_date = time_list.at(second_date)

        assert value_at_date == second_value

    def test_date_between_dates_in_the_list_returns_value_of_previous_date(self):
        time_list = kc.TimeList(int)
        time_list.set(kc.Date(2020, 2), 4)
        second_value = 5
        time_list.set(kc.Date(2020, 3), second_value)
        time_list.set(kc.Date(2020, 6), 6)

        date_in_between = kc.Date(2020, 5)
        value_at_date = time_list.at(date_in_between)

        assert value_at_date == second_value

    def test_date_after_last_date_returns_last_value(self):
        time_list = kc.TimeList(int)
        time_list.set(kc.Date(2020, 2), 4)
        time_list.set(kc.Date(2020, 3), 5)
        last_value = 10
        time_list.set(kc.Date(2020, 6), last_value)

        date_after = kc.Date(2020, 12)
        value_at_date = time_list.at(date_after)

        assert value_at_date == last_value

    @pytest.mark.parametrize("invalid_date", [None, (2020, 5), "2020-5"])
    def test_value_at_invalid_date_raises(self, invalid_date):
        time_list = kc.TimeList(int)
        time_list.set(kc.Date(2020, 2), 1)
        time_list.set(kc.Date(2020, 3), 2)
        with pytest.raises(TypeError):
            value = time_list.at(invalid_date)


class TestJsonInterface:

    @pytest.mark.parametrize("value_type, values", [
        (int, (2, 6)),
        (str, ("a", "b")),
        (bool, (True, False)),
        (float, (1.2e3, 4.56)),
        (list, ([1, 2], [3, 4])),
        (dict, ({}, {1: 2}))
    ])
    def test_to_dict_contains_2_keys_and_the_value_is_a_list(self, value_type, values):
        time_list = kc.TimeList(value_type)
        time_list.set(kc.Date(2020, 6), values[0])
        time_list.set(kc.Date(2020, 6), values[1])
        time_list_dict = time_list.to_dict()

        assert len(time_list_dict) == 2
        assert kc.TimeList.JSON_LIST_KEY in time_list_dict
        assert kc.TimeList.JSON_TYPE_KEY in time_list_dict
        assert isinstance(time_list_dict[kc.TimeList.JSON_LIST_KEY], list)
        assert time_list_dict[kc.TimeList.JSON_TYPE_KEY] == str(value_type)

    def test_from_dict_with_empty_raises(self):
        with pytest.raises(KeyError):
            time_list = kc.TimeList.from_dict({})

    def test_from_dict_missing_key_raises(self):
        with pytest.raises(KeyError):
            kc.TimeList.from_dict({"wrong_key": [{"d": {"ymd": [2020, 7, 1]}, "v": 2}]})

    @pytest.mark.parametrize("value", [1, "a", True, 1.55])
    def test_from_dict_base_type_with_value_class_set_raises(self, value):
        with pytest.raises(KeyError):
            kc.TimeList.from_dict(
                {kc.TimeList.JSON_LIST_KEY: [{"d": {"ymd": [2020, 7, 1]}, "v": value}]},
                kc.Date)





