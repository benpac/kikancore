import pytest

import kikancore as kc


class NonJsonInterface:
    """This class is a dummy example of a NonJsonInterface class to be used in the test.
    """
    def __init__(self):
        self.a = 12


class TestCreation:

    @pytest.mark.parametrize("date, value", [
        (kc.Date(2020, 6), "a"),
        (kc.Date(2020, 6, 12), "a"),
        (kc.Date(2020, 6), 1),
        (kc.Date(2020, 6), [1, 2, 3]),
        (kc.Date(2020, 6), {"a": 1, 3: [1, 2, 3]}),
        (kc.Date(2020, 6), None),
        (kc.Date(2020, 6), False),
    ])
    def test_valid_date_and_base_type_does_not_raise(self, date, value):
        time_node = kc.TimeNode(date, value)

    @pytest.mark.parametrize("date, value", [
        (kc.Date(2020, 6), kc.Date(2020, 7, 12))
    ])
    def test_valid_date_and_json_interface_does_not_raise(self, date, value):
        time_node = kc.TimeNode(date, value)

    @pytest.mark.parametrize("invalid_date, value", [
        (None, kc.Date(2020, 7, 12)),
        ("a", kc.Date(2020, 7, 12)),
        (3, kc.Date(2020, 7, 12)),
        (3, "a"),
        (3, {2: 3, 4: 5})
    ])
    def test_invalid_date_raises_type_error(self, invalid_date, value):
        with pytest.raises(TypeError):
            kc.TimeNode(invalid_date, value)

    @pytest.mark.parametrize("date, unaccepted_value", [
        (kc.Date(2020, 7, 2), NonJsonInterface())
    ])
    def test_non_base_type_or_json_interface_value_raises_type_error(
            self, date, unaccepted_value):
        with pytest.raises(TypeError):
            kc.TimeNode(date, unaccepted_value)


VALID_BASE_VALUES = ["A string", True, 42, 465.264, [12, 34], {1: 3, 3: 6}, None]
VALID_DATE = kc.Date(2020, 6)


class TestJsonInterface:

    class TestFromDict:
        VALID_DATE = kc.Date(2020, 6)

        def test_empty_dict_raises(self):
            with pytest.raises(KeyError):
                time_node = kc.TimeNode.from_dict({})

        def test_no_date_key_raises(self):
            with pytest.raises(KeyError):
                time_node = kc.TimeNode.from_dict({kc.TimeNode.JSON_VALUE: 1})

        def test_no_value_key_raises(self):
            with pytest.raises(KeyError):
                time_node = kc.TimeNode.from_dict(
                    {kc.TimeNode.JSON_DATE: VALID_DATE.to_dict()})

        def test_non_json_interface_value_class_raises(self):
            with pytest.raises(kc.CoreError):
                time_node = kc.TimeNode.from_dict(
                    {
                        kc.TimeNode.JSON_DATE: VALID_DATE.to_dict(),
                        kc.TimeNode.JSON_VALUE: {"non_json_interface": 12}
                    },
                    NonJsonInterface
                )

        @pytest.mark.parametrize("value", VALID_BASE_VALUES)
        def test_base_value_when_value_class_is_set_raises(self, value):
            with pytest.raises((TypeError, KeyError)):
                time_node = kc.TimeNode.from_dict(
                    {
                        kc.TimeNode.JSON_DATE: VALID_DATE.to_dict(),
                        kc.TimeNode.JSON_VALUE: value
                    },
                    kc.Date
                )

        @pytest.mark.parametrize("value", VALID_BASE_VALUES)
        def test_valid_dict_with_base_value_does_not_raise_and_date_and_value_are_set(self, value):
            time_node_dict = {
                kc.TimeNode.JSON_DATE: VALID_DATE.to_dict(),
                kc.TimeNode.JSON_VALUE: value
            }
            time_node = kc.TimeNode.from_dict(time_node_dict)
            assert time_node.date == VALID_DATE
            assert time_node.value == value

    class TestToDict:
        @pytest.mark.parametrize("value", VALID_BASE_VALUES)
        def test_to_dict_with_standard_value_has_2_elements_only_and_contains_date_and_value(
                self, value):
            time_node = kc.TimeNode(VALID_DATE, value)
            time_node_dict = time_node.to_dict()

            assert len(time_node_dict) == 2
            assert kc.TimeNode.JSON_DATE in time_node_dict
            assert kc.TimeNode.JSON_VALUE in time_node_dict
            assert time_node_dict[kc.TimeNode.JSON_VALUE] == value
            assert time_node_dict[kc.TimeNode.JSON_DATE] == VALID_DATE.to_dict()

        def test_to_dict_with_json_interface_object_has_date_and_object_to_dict(self):
            date_value = kc.Date(2020, 7, 12)
            time_node = kc.TimeNode(VALID_DATE, date_value)
            time_node_dict = time_node.to_dict()

            assert len(time_node_dict) == 2
            assert kc.TimeNode.JSON_DATE in time_node_dict
            assert kc.TimeNode.JSON_VALUE in time_node_dict
            assert time_node_dict[kc.TimeNode.JSON_VALUE] == date_value.to_dict()
            assert time_node_dict[kc.TimeNode.JSON_DATE] == VALID_DATE.to_dict()



