import pathlib

import kikancore.tools.directories as directories


def test_core_directory():
    value = directories.core_directory()
    assert value == pathlib.Path(pathlib.Path.home(), ".kikan")
