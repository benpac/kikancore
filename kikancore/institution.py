# Copyright Kikan 2020
# File created by Benjamin Paccaud - 04.06.20 <bpaccaud@gmail.com>


class Institution:
    """
    This class is the main container for the data of an institution. It is the main class
    containing the data.
    """
    def __init__(self, children, week, percent_rules, data):
        self.data = data
        self.children = children
        self.week = week
        self.percent_rules = percent_rules

    @staticmethod
    def load_from_file(filepath):
        """This function loads from a file on disk all the necessary data.

        :param filepath: the absolute filepath to the file containing the data.
        :return: an Institution
        """

        return Institution([], None, [], {})

    def save_to_string(self):
        raise NotImplementedError()

    def save_to_file(self, filepath):
        with open(filepath, 'w') as save_file:
            save_file.write(self.save_to_string())

