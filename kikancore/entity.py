# Copyright Kikan 2020
# File created by Benjamin Paccaud - 10.06.20 <bpaccaud@gmail.com>

"""
This file provides an abstract definition of an entity that represent objects in the kikancore
environment
"""


class AbstractEntity:
    def __init__(self, _id):
        self.id = _id

    @staticmethod
    def create_new(*args, **kwargs):
        raise NotImplementedError()
