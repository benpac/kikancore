import kikancore as kc


def go():
    institution = kc.Institution.load_from_file("")
    child = kc.Child.create_new({}, kc.Date(1991, 7, 4), kc.get_id(), [], kc.Date(2020, 6))
    percent_rule = kc.PercentRule()
    group = kc.Group.create_new("name", "type?", {})
    week = kc.Week([])
    day = kc.Day([])
    part_day = kc.PartDay("morning", [])
    presence = kc.Presence(0)


if __name__ == '__main__':
    go()
