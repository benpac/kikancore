What is this?
=============



API:

.. autoclass:: kikancore.Institution
   :members:

.. autoclass:: kikancore.Group
   :members:
