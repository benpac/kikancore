# Taken from https://python-packaging.readthedocs.io/en/latest/minimal.html

# continuous integration with TOX ? https://tox.readthedocs.io/en/latest/
# path to jenkins ?

from setuptools import setup


def readme():
      with open('README.rst') as f:
            return f.read()


setup(name='kikancore',
      version='0.1',
      description='Core of the kindergarten management system',
      url='https://bitbucket.org/benpac/kikancore/',
      author='Benjamin Paccaud',
      author_email='bpaccaud@gmail.com.com',
      license='MIT',
      classifiers=[
            'Development Status :: 1 - Planning',
            'Intended Audience :: Other Audience',
            'Natural Language :: English',
            'Programming Language :: Python :: 3.8',
            'Topic :: Office/Business :: Scheduling',
      ],
      keywords='kindergarten scheduling prevision',
      packages=['kikancore'],
      install_requires=[
      ],
      include_package_data=True,
      zip_safe=False)
