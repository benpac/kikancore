What is this?
*************

This repository is the Core of Kikan as I envision it.
It contains all the logic for the program to run.

It will be included in one route package that could be called from any place granted it is in the
path and all the sub element can be used.
 
 
What is Kikan
*************

Kikan is a program that aims to help kindergarten directors to plan and predict their
availability depending on the growth of the children, new comers, departures, etc.
It allows them to play, try and see the effect of some changes to have an idea of what the
future will look like, but due to the changing nature of the program and the uncertainty, it is
not aimed to be used for billing.
 
 
Description
***********

The main idea
=============

The principal idea behind Kikan is that a change is propagated to the future states without
changing the past.

The main structure
====================

All the descriptions here are a bit of a Brain dump. I might want to refine some ideas and go
another direction at some point.

Being for a kindergarten, this element will be the base of the structure.

Kindergarten
-------------

Attributes
^^^^^^^^^^

A kindergarten will have the following attribute:

* A set of attributes describing it in a very general manner
* A set of 'Group' definition
* A set of rules defining how a percentage is computed. From experience, there are 2 types:
   * Per part of day. Each part of day as a fixed value for the percentage.
   * Per mask. Some combination have some percentage but no one value per part can be determined.
* A 'Week' definition
   - The days of the week where children are welcome (1-7) and how each day is decomposed into
     subpart.
     Sub-parting is important to provide extra information to groups on top of any billing
     data, e.g. if a child stays for lunch, or very early in the morning or late in the
     afternoon.
* A list of 'Children'
 
All the attributes should have the ability to select the correct value for a specific month
 
Nice to have (maybe?):

* A List of employee ?
 
Features
^^^^^^^^

* Saves and restore
* Undo / Redo
* Add Child
* Add Group
* Modify general attributes
* add/change week definition
* add/change percentage rules
* get all the data for a specific month
* Compute the percentage for a specific month
* Aggregate data

Group
-----

A group serves primarily as a flag for child. A child will 'belong' to one or another group at
each month.

Attributes
^^^^^^^^^^

* name 
* type 
* anything? I'd like the rules to be able to reference the group thing
* id / reference

Features
^^^^^^^^^

* groups should be able to be associated to other groups, be filtered by them.
* could be that the percentrule tells the grouping for the number of places and there is a filter
  creation somewhere to groups results / queries for set of groups.

Child
-----
The core information is in there.

Attributes
^^^^^^^^^^^

* anything that does not change (name, address)
* date of birth (will be used for automatic group change)
* group (the group it belongs to). Variable that depends on the month it is looked at.
* week (its presences during the week). Variable that depends on the month it is looked at. Not
  sure if week of individual stuff? Unclear to me for now.


Features
^^^^^^^^^

* ??

Week
^^^^^^^^^
Represent a typical week

Attributes
^^^^^^^^^^

* days (list of Day, I guess?)

Features
^^^^^^^^^

when used with a percent rule, it should be able to return a value of the percentage of presence .

Day
^^^^^^^^^

Attributes
^^^^^^^^^^

* list of part of day PartDay
* number of part of days ? boh

Features
^^^^^^^^

PartDay
^^^^^^^^^

Attributes
^^^^^^^^^^

* Presence object
* name

Presence
^^^^^^^^^

Attributes
^^^^^^^^^^

* value of presence (numeric, 0 or 1)
* name
* abbreviation (one letter)

PercentRule
^^^^^^^^^^^

this is an object use to compute the percentage of presence of a child based on their presence in
a week. It should describe how things are computed. I see 2 major way of computing it, either by
mask -> each combination of 1 and 0 for a day gives a value and they are explicitely given or
by value for each part of day, when we can determine that one specific part of day will be of
a given value. It will apply to a specific week type and there might be different rules for
different groups

Attributes
^^^^^^^^^^


Feature
^^^^^^^^^

given a week, should output a number between 0 and 1.


Implementation
==============

Most of the objects should have a evalAt(Date) function that would go through and return a proxy
object that only has the value for the date and not everything. The proxy object is used for
display. Quid of update? Reference to the object? also
 
How is it stored on disk ? I'd love to have something that is readable, because it can make
things easy I think. Also to create or update things manually.
Should a db be used? I'd rather just have something to disk, and everything to memory. I believe
it should not be too large to stay in the ram (famous last word)

To have something on disk, every item should have a way of serializing and deserializing itself.
I should read more about this

I would like to separate the data from the operations on it, and have an intermediate class that
perform the various filtering and computation that I want.
This object should have the evalAt function, but each data object should be of a specific type
so that it can be handled. The basic idea is that it would be a linked list (or just a list) and
you go through it until the month is bigger or the end.
  

Things that should be possible
==============================

But maybe part of the view instead of this core.
 
* View and print a month
* View and print summary for a set of months


CI
==

https://raspberrytips.com/install-jenkins-raspberry-pi/
